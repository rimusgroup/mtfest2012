package com.mtfest.controller {


	import com.mtfest.application.AppStates;
	import com.mtfest.application.SoundType;
	import com.mtfest.application.message.SetAppState;
	import com.mtfest.model.feedpoller.message.PollerInitialised;
	import com.mtfest.model.soundcue.SoundCue;
	import com.mtfest.model.soundcue.entity.Track;
	import com.mtfest.view.player.entity.PlayerItem;
	import com.mtfest.view.player.message.SetPlayerItems;

	import mx.collections.ArrayCollection;


	/**
	 * @author rimaskrivickas
	 */
	public class PollerInitialisedCommand {


		[MessageDispatcher]
		public var dispatch : Function;

		[Inject]
		public var soundCue : SoundCue;


		public function execute ( message : PollerInitialised ) : void {
			dispatch ( new SetAppState ( AppStates.PLAYER ) );

			soundCue.addTrack ( new Track ( SoundType.BEAT, new Array ( "kick_01", "kick_02", "kick_03", "kick_04", "kick_05", "kick_06", "kick_07", "kick_08" ), new Array ( [ 1, 0, 0, 0 ], [ 1, 0, 0 ], [ 1, 0 ] ) ) );
			soundCue.addTrack ( new Track ( SoundType.BASS, new Array ( "bass_01", "bass_02", "bass_03", "bass_04", "bass_05", "bass_06", "bass_07", "bass_08" ), null, new Array ( 4, 5, 6, 7 ) ) );
			soundCue.addTrack ( new Track ( SoundType.HIHAT, new Array ( "hihat_01", "hihat_02", "hihat_03", "hihat_04", "hihat_05", "hihat_06", "hihat_07", "hihat_08" ), new Array ( [ 0, 0, 1, 0 ], [ 1, 1, 1, 0 ], [ 0, 0, 1 ], [ 1 ] ) ) );
			soundCue.addTrack ( new Track ( SoundType.SNARE, new Array ( "snare_01", "snare_02", "snare_03", "snare_04", "snare_05", "snare_06", "snare_07", "snare_08" ), new Array ( [ 0, 0, 1, 0, 0, 1, 0, 0 ], [ 0, 0, 0, 0, 1, 0, 0, 0 ] ) ) );
			soundCue.addTrack ( new Track ( SoundType.SYNTH, new Array ( "synth_01", "synth_02", "synth_03", "synth_04", "synth_05", "synth_06", "synth_07", "synth_08" ), null, new Array ( 4, 5, 6, 7 ) ) );
			soundCue.addTrack ( new Track ( SoundType.PERC, new Array ( "percussion_01", "percussion_02", "percussion_03", "percussion_04", "percussion_05", "percussion_06", "percussion_07", "percussion_08" ), new Array ( [ 1, 0, 0, 0 ], [ 1, 1, 0, 0, 0, 1, 0, 0 ], [ 0, 0, 1, 1, 0, 0, 0, 1 ], [ 1, 0, 0 ], [ 1, 0 ] ) ) );
			soundCue.addTrack ( new Track ( SoundType.VOX, new Array ( "vox_01", "vox_02", "vox_03", "vox_04", "vox_05", "vox_06", "vox_07", "vox_08" ), null, new Array ( 5, 7, 9 ) ) );
			soundCue.addTrack ( new Track ( SoundType.FX, new Array ( "fx_01", "fx_02", "fx_03", "fx_04", "fx_05", "fx_06", "fx_07", "fx_08" ), null, new Array ( 7, 9, 16, 21 ) ) );

			var playerItems : ArrayCollection = new ArrayCollection ();
			playerItems.addItem ( new PlayerItem ( SoundType.BEAT ) );
			playerItems.addItem ( new PlayerItem ( SoundType.BASS ) );
			playerItems.addItem ( new PlayerItem ( SoundType.HIHAT ) );
			playerItems.addItem ( new PlayerItem ( SoundType.SNARE ) );
			playerItems.addItem ( new PlayerItem ( SoundType.SYNTH ) );
			playerItems.addItem ( new PlayerItem ( SoundType.PERC ) );
			playerItems.addItem ( new PlayerItem ( SoundType.VOX ) );
			playerItems.addItem ( new PlayerItem ( SoundType.FX ) );

			dispatch ( new SetPlayerItems ( playerItems ) );
			
			soundCue.play();
		}
	}
}