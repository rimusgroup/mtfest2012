package com.mtfest.controller {


	import com.mtfest.application.AppStates;
	import com.mtfest.application.message.SetAppState;
	import com.mtfest.model.feedpoller.FeedPoller;
	import com.mtfest.view.setuppage.message.SetupComplete;
	
	
	/**
	 * @author rimaskrivickas
	 */
	public class SetupCompleteCommand {

		
		[MessageDispatcher]
		public var dispatch : Function;
		
		[Inject]
		public var commandFeedManager : FeedPoller;
		
		
		public function execute ( message : SetupComplete ) : void {
			commandFeedManager.pollingTag = message.tag;
			dispatch ( new SetAppState(AppStates.INITIALISING));
			if ( !commandFeedManager.isRunning ) commandFeedManager.start();
		}	
	}
}