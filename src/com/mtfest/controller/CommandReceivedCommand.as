package com.mtfest.controller {


	import com.mtfest.view.player.PlayerItemStates;
	import com.mtfest.view.player.message.SetPlayerItemState;
	import com.mtfest.application.PlaybackCommand;
	import com.mtfest.application.SoundPool;
	import com.mtfest.model.feedpoller.message.CommandReceived;
	import com.mtfest.model.soundcue.SoundCue;


	/**
	 * @author rimaskrivickas
	 */
	public class CommandReceivedCommand {


		[MessageDispatcher]
		public var dispatch : Function;

		[Inject]
		public var soundCue : SoundCue;


		public function execute ( message : CommandReceived ) : void {
			var item : String;
			switch(message.command) {
				case PlaybackCommand.START:
					soundCue.startSound ( message.target );
					dispatch ( new SetPlayerItemState ( message.target, PlayerItemStates.PENDING, message.initiatorName, message.initiatorImage ) );
					break;
				case PlaybackCommand.END:
					soundCue.endSound ( message.target );
					dispatch ( new SetPlayerItemState ( message.target, PlayerItemStates.PENDING, message.initiatorName, message.initiatorImage ) );
					break;
				case PlaybackCommand.RANDOMISE:
					soundCue.randomiseSound ( message.target );
					dispatch ( new SetPlayerItemState ( message.target, PlayerItemStates.PENDING, message.initiatorName, message.initiatorImage ) );
					break;
				case PlaybackCommand.BPM:
					if ( !isNaN ( Number ( message.target ) )) soundCue.bpm = Number ( message.target );
					break;
				case PlaybackCommand.PLAY:
					soundCue.play ();
					for each ( item in soundCue.getAllTrackIDs () ) {
						dispatch ( new SetPlayerItemState ( item, PlayerItemStates.OFF, message.initiatorName, message.initiatorImage ) );
					}
					for each ( item in soundCue.getAllActiveTrackIDs() ) {
						dispatch ( new SetPlayerItemState ( item, PlayerItemStates.ON, message.initiatorName, message.initiatorImage ) );
					}
					break;
				case PlaybackCommand.STOP:
					soundCue.stop ();
					for each ( item in soundCue.getAllTrackIDs () ) {
						dispatch ( new SetPlayerItemState ( item, PlayerItemStates.OFF, message.initiatorName, message.initiatorImage ) );
					}
					break;
			}
		}
	}
}