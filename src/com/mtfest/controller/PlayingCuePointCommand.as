package com.mtfest.controller {


	import com.mtfest.model.soundcue.SoundCue;
	import com.mtfest.utils.Utils;
	import com.mtfest.model.soundcue.message.PlayingCuePoint;
	import com.mtfest.view.background.message.UpdateBackground;


	/**
	 * @author rimaskrivickas
	 */
	public class PlayingCuePointCommand {


		[MessageDispatcher]
		public var dispatch : Function;
		
		[Inject]
		public var soundCue : SoundCue;


		public function execute ( message : PlayingCuePoint ) : void {
			if ( message.currentNote == 1 ) {
				if ( soundCue.getAllActiveTrackIDs().length!= 0 ) dispatch ( new UpdateBackground () );
			} else {
				if ( message.currentNote % Utils.randRange ( 0, 5 ) == 0 ) {
					if ( soundCue.getAllActiveTrackIDs().length!= 0 ) dispatch ( new UpdateBackground () );
				}
			}
		}
	}
}