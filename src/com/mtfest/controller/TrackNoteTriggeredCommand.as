package com.mtfest.controller {


	import com.mtfest.model.soundcue.message.TrackNoteTriggered;
	import com.mtfest.view.player.message.TriggerPlayerItem;
	
	
	/**
	 * @author rimaskrivickas
	 */
	public class TrackNoteTriggeredCommand {

		
		[MessageDispatcher]
		public var dispatch : Function;
		
		
		public function execute ( message : TrackNoteTriggered ) : void {
			dispatch( new TriggerPlayerItem ( message.trackID ) );
		}	
	}
}