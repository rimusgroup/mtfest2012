package com.mtfest.controller {


	import com.mtfest.model.soundcue.SoundCue;
	import com.mtfest.model.soundcue.message.NewLoopStarted;
	import com.mtfest.view.player.message.SetPlayerItemState;


	/**
	 * @author rimaskrivickas
	 */
	public class NewLoopStartedCommand {


		[MessageDispatcher]
		public var dispatch : Function;

		[Inject]
		public var soundCue : SoundCue;


		public function execute ( message : NewLoopStarted ) : void {
			var item : String;
			for each ( item in soundCue.getAllTrackIDs () ) {
				dispatch ( new SetPlayerItemState ( item, "off" ) );
			}
			for each ( item in soundCue.getAllActiveTrackIDs () ) {
				dispatch ( new SetPlayerItemState ( item, "on" ) );
			}
		}
	}
}