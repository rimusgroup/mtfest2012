package com.mtfest.application {


	import flash.media.Sound;
	import flash.utils.Dictionary;


	/**
	 * @author rimaskrivickas
	 */
	public class SoundPool {


		private static var sounds : Dictionary = new Dictionary ();
		
		// ---------------------------------------------------------------------------------------
		// Bass
		// ---------------------------------------------------------------------------------------
		[Embed(source="/sounds/bass/bass_01.mp3")]
		public static var bass_01 : Class;
		
		[Embed(source="/sounds/bass/bass_02.mp3")]
		public static var bass_02 : Class;
		
		[Embed(source="/sounds/bass/bass_03.mp3")]
		public static var bass_03 : Class;
		
		[Embed(source="/sounds/bass/bass_04.mp3")]
		public static var bass_04 : Class;
		
		[Embed(source="/sounds/bass/bass_05.mp3")]
		public static var bass_05 : Class;
		
		[Embed(source="/sounds/bass/bass_06.mp3")]
		public static var bass_06 : Class;
		
		[Embed(source="/sounds/bass/bass_07.mp3")]
		public static var bass_07 : Class;
		
		[Embed(source="/sounds/bass/bass_08.mp3")]
		public static var bass_08 : Class;
		
		
		// ---------------------------------------------------------------------------------------
		// Snare
		// ---------------------------------------------------------------------------------------
		[Embed(source="/sounds/snare/snare_01.mp3")]
		public static var snare_01 : Class;
		
		[Embed(source="/sounds/snare/snare_02.mp3")]
		public static var snare_02 : Class;
		
		[Embed(source="/sounds/snare/snare_03.mp3")]
		public static var snare_03 : Class;
		
		[Embed(source="/sounds/snare/snare_04.mp3")]
		public static var snare_04 : Class;
		
		[Embed(source="/sounds/snare/snare_05.mp3")]
		public static var snare_05 : Class;
		
		[Embed(source="/sounds/snare/snare_06.mp3")]
		public static var snare_06 : Class;
		
		[Embed(source="/sounds/snare/snare_07.mp3")]
		public static var snare_07 : Class;
		
		[Embed(source="/sounds/snare/snare_08.mp3")]
		public static var snare_08 : Class;
		
		// ---------------------------------------------------------------------------------------
		// Hihats
		// ---------------------------------------------------------------------------------------
		[Embed(source="/sounds/hihat/hihat_01.mp3")]
		public static var hihat_01 : Class;
		
		[Embed(source="/sounds/hihat/hihat_02.mp3")]
		public static var hihat_02 : Class;
		
		[Embed(source="/sounds/hihat/hihat_03.mp3")]
		public static var hihat_03 : Class;
		
		[Embed(source="/sounds/hihat/hihat_04.mp3")]
		public static var hihat_04 : Class;
		
		[Embed(source="/sounds/hihat/hihat_05.mp3")]
		public static var hihat_05 : Class;
		
		[Embed(source="/sounds/hihat/hihat_06.mp3")]
		public static var hihat_06 : Class;
		
		[Embed(source="/sounds/hihat/hihat_07.mp3")]
		public static var hihat_07 : Class;
		
		[Embed(source="/sounds/hihat/hihat_08.mp3")]
		public static var hihat_08 : Class;
		
		
		// ---------------------------------------------------------------------------------------
		// Vocals
		// ---------------------------------------------------------------------------------------
		[Embed(source="/sounds/vox/vox_01.mp3")]
		public static var vox_01 : Class;
		
		[Embed(source="/sounds/vox/vox_02.mp3")]
		public static var vox_02 : Class;
		
		[Embed(source="/sounds/vox/vox_03.mp3")]
		public static var vox_03 : Class;
		
		[Embed(source="/sounds/vox/vox_04.mp3")]
		public static var vox_04 : Class;
		
		[Embed(source="/sounds/vox/vox_05.mp3")]
		public static var vox_05 : Class;
		
		[Embed(source="/sounds/vox/vox_06.mp3")]
		public static var vox_06 : Class;
		
		[Embed(source="/sounds/vox/vox_07.mp3")]
		public static var vox_07 : Class;
		
		[Embed(source="/sounds/vox/vox_08.mp3")]
		public static var vox_08 : Class;
		
		
		// ---------------------------------------------------------------------------------------
		// Kicks
		// ---------------------------------------------------------------------------------------
		[Embed(source="/sounds/kick/kick_01.mp3")]
		public static var kick_01 : Class;

		[Embed(source="/sounds/kick/kick_02.mp3")]
		public static var kick_02 : Class;

		[Embed(source="/sounds/kick/kick_03.mp3")]
		public static var kick_03 : Class;

		[Embed(source="/sounds/kick/kick_04.mp3")]
		public static var kick_04 : Class;

		[Embed(source="/sounds/kick/kick_05.mp3")]
		public static var kick_05 : Class;

		[Embed(source="/sounds/kick/kick_06.mp3")]
		public static var kick_06 : Class;

		[Embed(source="/sounds/kick/kick_07.mp3")]
		public static var kick_07 : Class;

		[Embed(source="/sounds/kick/kick_08.mp3")]
		public static var kick_08 : Class;
		
		
		// ---------------------------------------------------------------------------------------
		// Percussion
		// ---------------------------------------------------------------------------------------
		[Embed(source="/sounds/percussion/percussion_01.mp3")]
		public static var percussion_01 : Class;
		
		[Embed(source="/sounds/percussion/percussion_02.mp3")]
		public static var percussion_02 : Class;
		
		[Embed(source="/sounds/percussion/percussion_03.mp3")]
		public static var percussion_03 : Class;
		
		[Embed(source="/sounds/percussion/percussion_04.mp3")]
		public static var percussion_04 : Class;
		
		[Embed(source="/sounds/percussion/percussion_05.mp3")]
		public static var percussion_05 : Class;
		
		[Embed(source="/sounds/percussion/percussion_06.mp3")]
		public static var percussion_06 : Class;
		
		[Embed(source="/sounds/percussion/percussion_07.mp3")]
		public static var percussion_07 : Class;
		
		[Embed(source="/sounds/percussion/percussion_08.mp3")]
		public static var percussion_08 : Class;
		
		
		// ---------------------------------------------------------------------------------------
		// Synths
		// ---------------------------------------------------------------------------------------
		[Embed(source="/sounds/synth/synth_01.mp3")]
		public static var synth_01 : Class;
		
		[Embed(source="/sounds/synth/synth_02.mp3")]
		public static var synth_02 : Class;
		
		[Embed(source="/sounds/synth/synth_03.mp3")]
		public static var synth_03 : Class;
		
		[Embed(source="/sounds/synth/synth_04.mp3")]
		public static var synth_04 : Class;
		
		[Embed(source="/sounds/synth/synth_05.mp3")]
		public static var synth_05 : Class;
		
		[Embed(source="/sounds/synth/synth_06.mp3")]
		public static var synth_06 : Class;
		
		[Embed(source="/sounds/synth/synth_07.mp3")]
		public static var synth_07 : Class;
		
		[Embed(source="/sounds/synth/synth_08.mp3")]
		public static var synth_08 : Class;
		
		
		// ---------------------------------------------------------------------------------------
		// FX
		// ---------------------------------------------------------------------------------------
		[Embed(source="/sounds/fx/fx_01.mp3")]
		public static var fx_01 : Class;
		
		[Embed(source="/sounds/fx/fx_02.mp3")]
		public static var fx_02 : Class;
		
		[Embed(source="/sounds/fx/fx_03.mp3")]
		public static var fx_03 : Class;
		
		[Embed(source="/sounds/fx/fx_04.mp3")]
		public static var fx_04 : Class;
		
		[Embed(source="/sounds/fx/fx_05.mp3")]
		public static var fx_05 : Class;
		
		[Embed(source="/sounds/fx/fx_06.mp3")]
		public static var fx_06 : Class;
		
		[Embed(source="/sounds/fx/fx_07.mp3")]
		public static var fx_07 : Class;
		
		[Embed(source="/sounds/fx/fx_08.mp3")]
		public static var fx_08 : Class;
		

		// ---------------------------------------------------------------------------------------
		// Public API
		// ---------------------------------------------------------------------------------------
		public static function play ( id : String ) : void {
			var sound : Sound;
			if ( sounds [ id ] == undefined ) {
				sounds [ id ] = new (SoundPool[id] as Class)();
			}
			sound = sounds [ id ] as Sound;
			sound.play ();
		}
	}
}