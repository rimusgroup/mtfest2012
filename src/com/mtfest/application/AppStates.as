package com.mtfest.application {


	/**
	 * @author rimaskrivickas
	 */
	public class AppStates {


		public static const SETUP : String = "setup";

		public static const INITIALISING : String = "initialising";

		public static const PLAYER : String = "player";
	}
}
