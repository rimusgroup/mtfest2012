package com.mtfest.application {


	/**
	 * @author rimaskrivickas
	 */
	public class PlaybackCommand {


		public static const START : String = "start";

		public static const END : String = "end";

		public static const RANDOMISE : String = "randomise";

		public static const BPM : String = "bpm";

		public static const PLAY : String = "play";

		public static const STOP : String = "stop";
	}
}
