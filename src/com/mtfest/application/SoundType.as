package com.mtfest.application {


	/**
	 * @author rimaskrivickas
	 */
	public class SoundType {


		public static const BEAT : String = "beat";

		public static const SNARE : String = "snare";

		public static const HIHAT : String = "hihat";

		public static const BASS : String = "bass";

		public static const VOX : String = "vox";

		public static const PERC : String = "perc";

		public static const SYNTH : String = "synth";
		
		public static const FX : String = "fx";

	}
}
