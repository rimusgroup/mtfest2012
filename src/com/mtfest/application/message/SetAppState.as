package com.mtfest.application.message {


	/**
	 * @author rimaskrivickas
	 */
	public class SetAppState {


		private var _state : String;


		public function SetAppState ( state : String ) {
			_state = state;
		}


		public function get state () : String {
			return _state;
		}

		
	}
}
