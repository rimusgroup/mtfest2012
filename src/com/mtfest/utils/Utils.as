package com.mtfest.utils {


	public class Utils {


		public static function randRange ( min : Number, max : Number ) : Number {
			var randomNumber : Number = Math.floor ( Math.random () * ( max - min + 1 ) ) + min;

			return randomNumber;
		}
	}
}