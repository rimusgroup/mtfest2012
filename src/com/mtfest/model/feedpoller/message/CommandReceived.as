package com.mtfest.model.feedpoller.message {




	/**
	 * @author rimaskrivickas
	 */
	public class CommandReceived {


		private var _target : String;

		private var _command : String;

		private var _initiatorName : String;

		private var _initiatorImage : String;


		public function CommandReceived ( target : String, command : String, initiatorName : String = null, initiatorImage : String = null ) {
			_initiatorImage = initiatorImage;
			_initiatorName = initiatorName;
			_command = command;
			_target = target;
		}


		public function get target () : String {
			return _target;
		}


		public function get command () : String {
			return _command;
		}


		public function get initiatorName () : String {
			return _initiatorName;
		}


		public function get initiatorImage () : String {
			return _initiatorImage;
		}

		
	}
}
