package com.mtfest.model.feedpoller {


	import com.adobe.serialization.json.JSON;
	import com.mtfest.application.PlaybackCommand;
	import com.mtfest.model.feedpoller.message.CommandReceived;
	import com.mtfest.model.feedpoller.message.PollerInitialised;

	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.clearInterval;
	import flash.utils.clearTimeout;
	import flash.utils.setInterval;
	import flash.utils.setTimeout;


	/**
	 * @author rimaskrivickas
	 */
	public class FeedPoller {


		public static const BASE_QUERY : String = "http://search.twitter.com/search.json?q=";

		[MessageDispatcher]
		public var dispatch : Function;

		public var pollInterval : Number = 1000;

		public var timeout : Number = 25000;

		public var knownTracks : Array = [];

		private var _isRunning : Boolean;

		private var _pollingTag : String = "";

		private var lastID : String;

		private var _initialised : Boolean;

		private var requestInProgress : Boolean;

		private var pollTimer : int;

		private var timeoutTimer : int;

		private var urlLoader : URLLoader;

		private var urlRequest : URLRequest;


		// ---------------------------------------------------------------------------------------
		// Constructor
		// ---------------------------------------------------------------------------------------
		public function FeedPoller () {
			urlLoader = new URLLoader ();
			urlRequest = new URLRequest ();
			urlLoader.addEventListener ( Event.COMPLETE, onURLLoadComplete );
			urlLoader.addEventListener ( SecurityErrorEvent.SECURITY_ERROR, onURLLoadSecurityError );
			urlLoader.addEventListener ( IOErrorEvent.IO_ERROR, onURLLoadIOError );
		}


		// ---------------------------------------------------------------------------------------
		// Public API
		// ---------------------------------------------------------------------------------------
		public function get pollingTag () : String {
			return _pollingTag;
		}


		public function set pollingTag ( value : String ) : void {
			_pollingTag = value;
		}


		public function get isRunning () : Boolean {
			return _isRunning;
		}


		public function start () : void {
			if ( !isRunning ) {
				poll ();
				pollTimer = setInterval ( poll, pollInterval );
				_isRunning = true;
			}
		}


		public function stop () : void {
			clearInterval ( pollTimer );
			_isRunning = false;
			resetPoll ();
		}


		// ---------------------------------------------------------------------------------------
		// Internal logic
		// ---------------------------------------------------------------------------------------
		private function poll () : void {
			if ( !requestInProgress && _pollingTag != null && _pollingTag != "" ) {
				requestInProgress = true;
				timeoutTimer = setTimeout ( resetPoll, timeout );
				urlRequest.url = makeURL ();
				urlLoader.load ( urlRequest );
			}
		}


		private function resetPoll () : void {
			try {
				urlLoader.close ();
			} catch ( error : Error ) {
			}
			clearTimeout ( timeoutTimer );
			requestInProgress = false;
		}


		private function makeURL () : String {
			if (lastID == null) return BASE_QUERY + "%23" + pollingTag + "&rpp=1";
			return BASE_QUERY + "%23" + pollingTag + "&since_id=" + lastID;
		}


		// ---------------------------------------------------------------------------------------
		// Event handlers
		// ---------------------------------------------------------------------------------------
		private function onURLLoadIOError ( event : IOErrorEvent ) : void {
			requestInProgress = false;
		}


		private function onURLLoadSecurityError ( event : SecurityErrorEvent ) : void {
			requestInProgress = false;
		}


		private function onURLLoadComplete ( event : Event ) : void {
			var result : Array = JSON.decode ( urlLoader.data )["results"] as Array;
			if ( result.length >= 1 ) {
				lastID = String ( result [ result.length - 1 ]["id_str"] );

				if ( !_initialised ) {
					dispatch ( new PollerInitialised () );
					_initialised = true;
				} else {
					parseCommands ( result );
				}
			}
			requestInProgress = false;
		}


		private function parseCommands ( list : Array ) : void {
			for each ( var object : Object in list ) {
				var initiatorName : String = object["from_user"];
				var initiatorImage : String = object["profile_image_url"];
				var text : String = String ( object["text"] );
				for each ( var match : String in text.match ( /(\s)(hit|end|mix|bpm|play|stop)(\s)(\w*)/gi ) ) {
					var values : Array = match.split ( " " );
					var target : String = "";
					var command : String;
					switch(values[1]) {
						// Track only
						case "hit":
							target = ( knownTracks.indexOf ( values[2] ) != -1 ) ? values[2] : null;
							command = PlaybackCommand.START;
							break;
						case "end":
							target = ( knownTracks.indexOf ( values[2] ) != -1 ) ? values[2] : null;
							command = PlaybackCommand.END;
							break;
						case "mix":
							target = ( knownTracks.indexOf ( values[2] ) != -1 ) ? values[2] : null;
							command = PlaybackCommand.RANDOMISE;
							break;
						// General
						case "bpm":
							target = values[2];
							command = PlaybackCommand.BPM;
							break;
						case "play":
							command = PlaybackCommand.PLAY;
							break;
						case "stop":
							command = PlaybackCommand.STOP;
							break;
					}
					if ( target != null ) dispatch ( new CommandReceived ( target, command, initiatorName, initiatorImage ) );
				}
			}
		}
	}
}
