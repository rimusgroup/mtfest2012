package com.mtfest.model.soundcue.event {


	import flash.events.Event;
	

	/**
	 * @author rimaskrivickas
	 */
	public class TrackEvent extends Event {
		
		
		public static const NOTE_TRIGGER : String = "noteTrigger";

		private var _trackID : String;
		
		
		public function TrackEvent ( type : String, trackID : String ) {
			_trackID = trackID;
			super ( type, true, false );
		}
		
		
		override public function clone ( ) : Event {
			return new TrackEvent ( type, _trackID );
		}


		public function get trackID () : String {
			return _trackID;
		}	
	}
}
