package com.mtfest.model.soundcue.entity {


	import com.mtfest.model.soundcue.event.TrackEvent;

	import flash.events.EventDispatcher;

	import com.mtfest.application.SoundPool;
	import com.mtfest.utils.Utils;


	/**
	 * @author rimaskrivickas
	 */
	public class Track extends EventDispatcher {


		private var _id : String;

		private var _possibleSounds : Array;

		private var _isActive : Boolean;

		private var _loopLength : uint;

		private var notes : Array;

		private var sounds : Array;

		private var suggestedPatterns : Array;

		private var noteGapRange : Array;


		// ---------------------------------------------------------------------------------------
		// Constructor
		// ---------------------------------------------------------------------------------------
		public function Track ( id : String, possibleSounds : Array, suggestedPatterns : Array = null, noteGapRange : Array = null ) {
			_id = id;
			_possibleSounds = possibleSounds;
			if ( suggestedPatterns == null ) suggestedPatterns = [];
			this.suggestedPatterns = suggestedPatterns;
			if ( noteGapRange == null ) noteGapRange = [];
			this.noteGapRange = noteGapRange;
		}


		// ---------------------------------------------------------------------------------------
		// Public getters/setters
		// ---------------------------------------------------------------------------------------
		public function get id () : String {
			return _id;
		}


		public function get possibleSounds () : Array {
			return _possibleSounds;
		}


		public function get loopLength () : uint {
			return _loopLength;
		}


		public function get isActive () : Boolean {
			return _isActive;
		}


		public function set isActive ( isActive : Boolean ) : void {
			_isActive = isActive;
		}


		// ---------------------------------------------------------------------------------------
		// Public API
		// ---------------------------------------------------------------------------------------
		public function makeLoop ( loopLength : uint ) : void {
			_loopLength = loopLength;
			setRandomSounds ();
			setNotes ();
		}


		public function playNote ( noteNumber : uint ) : void {
			if ( _isActive && noteNumber <= loopLength && notes [noteNumber - 1] == 1 ) {
				SoundPool.play ( sounds [ noteNumber - 1 ] as String );
				dispatchEvent ( new TrackEvent ( TrackEvent.NOTE_TRIGGER, id ) );
			}
		}


		// ---------------------------------------------------------------------------------------
		// Internal logic
		// ---------------------------------------------------------------------------------------
		private function setRandomSounds () : void {
			sounds = new Array ();
			for ( var i : uint = 0; i < loopLength; i++ ) {
				sounds [ i ] = possibleSounds [ Utils.randRange ( 0, possibleSounds.length - 1 ) ] as String ;
			}
		}


		private function setNotes () : void {
			if ( suggestedPatterns.length >= 1 ) {
				if ( Utils.randRange ( 0, 1 ) == 1 ) {
					notes = new Array ();
					var suggestedPattern : Array = suggestedPatterns [ Utils.randRange ( 0, suggestedPatterns.length - 1 )] as Array;
					var j : uint = 0;
					for ( var i : uint = 0; i < loopLength; i++ ) {
						if ( j >= suggestedPattern.length ) j = 0;
						notes [ i ] = suggestedPattern [ j ];
						j++;
					}
				} else {
					setRandomNotes ();
				}
			} else {
				setRandomNotes ();
			}
		}


		private function setRandomNotes () : void {
			notes = new Array ();
			var noteGap : Number = noteGapRange[Utils.randRange ( 0, noteGapRange.length - 1 )] as Number;
			for ( var i : uint = 0; i < loopLength; i++ ) {
				if ( noteGapRange.length == 0 ) {
					notes [ i ] = Utils.randRange ( 0, 1 );
				} else {
					notes [ i ] = ( i % noteGap == 0 ) ? Utils.randRange ( 0, 1 ) : 0;
				}
			}
		}
	}
}
