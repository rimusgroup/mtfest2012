package com.mtfest.model.soundcue {


	import com.mtfest.model.soundcue.message.TrackNoteTriggered;
	import com.mtfest.model.soundcue.event.TrackEvent;
	import com.mtfest.model.soundcue.entity.Track;
	import com.mtfest.model.soundcue.message.NewLoopStarted;
	import com.mtfest.model.soundcue.message.PlayingCuePoint;

	import flash.utils.clearInterval;
	import flash.utils.setInterval;


	/**
	 * @author rimaskrivickas
	 */
	public class SoundCue {


		private static const BAR : Number = 4;

		private static const MINUTE_IN_MILLISECONDS : Number = 60000 / BAR;

		[MessageDispatcher]
		public var dispatch : Function;

		private var _isPlaying : Boolean;

		private var _bpm : Number;

		private var beatDuration : Number;

		private var playheadInterval : int;

		private var _loopLength : uint;

		private var _currentNote : uint;

		private var tracks : Vector.<Track>;

		private var startSoundQueue : Array;

		private var endSoundQueue : Array;

		private var randomiseSoundQueue : Array;


		public function SoundCue () {
			bpm = 96;
			loopLength = 8;
			tracks = new Vector.<Track> ();
			startSoundQueue = new Array ();
			endSoundQueue = new Array ();
			randomiseSoundQueue = new Array ();
		}


		// ---------------------------------------------------------------------------------------
		// Public getters/setters
		// ---------------------------------------------------------------------------------------
		public function get bpm () : Number {
			return _bpm;
		}


		public function set bpm ( bpm : Number ) : void {
			_bpm = bpm;
			beatDuration = MINUTE_IN_MILLISECONDS / bpm;
			if ( isPlaying ) {
				updatePlayheadInterval ();
			}
		}


		public function get isPlaying () : Boolean {
			return _isPlaying;
		}


		public function get loopLength () : uint {
			return _loopLength;
		}


		public function set loopLength ( loopLength : uint ) : void {
			_loopLength = loopLength * BAR;
		}


		public function get currentNote () : uint {
			return _currentNote;
		}


		// ---------------------------------------------------------------------------------------
		// Public API
		// ---------------------------------------------------------------------------------------
		public function play () : void {
			if ( !_isPlaying ) {
				playheadInterval = setInterval ( playCue, beatDuration );
				_isPlaying = true;
				_currentNote = 0;
			}
		}


		public function stop () : void {
			clearInterval ( playheadInterval );
			_isPlaying = false;
			for each ( var track : Track in tracks ) {
				track.isActive = false;
			}
		}


		public function addTrack ( track : Track ) : void {
			track.makeLoop ( loopLength );
			track.addEventListener ( TrackEvent.NOTE_TRIGGER, onNoteTrigger );
			tracks.push ( track );
		}


		public function startSound ( trackID : String ) : void {
			var track : Track = getTrackByID ( trackID );
			if ( track != null ) {
				if (!track.isActive) startSoundQueue.push ( track );
			}
		}


		public function endSound ( trackID : String ) : void {
			var track : Track = getTrackByID ( trackID );
			if ( track != null ) {
				if (track.isActive) endSoundQueue.push ( track );
			}
		}


		public function randomiseSound ( trackID : String ) : void {
			var track : Track = getTrackByID ( trackID );
			if ( track != null ) {
				randomiseSoundQueue.push ( track );
			}
		}


		public function getAllActiveTrackIDs () : Array {
			var result : Array = new Array ();
			for each ( var track : Track in tracks ) {
				if ( track.isActive == true ) result.push ( track.id );
			}
			return result;
		}


		public function getAllTrackIDs () : Array {
			var result : Array = new Array ();
			for each ( var track : Track in tracks ) {
				result.push ( track.id );
			}
			return result;
		}


		// ---------------------------------------------------------------------------------------
		// Event handlers
		// ---------------------------------------------------------------------------------------
		private function onNoteTrigger ( event : TrackEvent ) : void {
			dispatch ( new TrackNoteTriggered ( event.trackID ) );
		}


		// ---------------------------------------------------------------------------------------
		// Internal logic
		// ---------------------------------------------------------------------------------------
		private function playCue () : void {
			_currentNote++;
			if (_currentNote > loopLength) _currentNote = 1;
			if ( currentNote == 1 ) {
				processPlaybackCommands ();
				dispatch ( new NewLoopStarted () );
			}
			for each ( var track : Track in tracks ) {
				track.playNote ( currentNote );
			}
			dispatch ( new PlayingCuePoint ( currentNote ) );
		}


		private function processPlaybackCommands () : void {
			var track : Track;

			for each ( track in startSoundQueue ) {
				track.isActive = true;
			}
			startSoundQueue = new Array ();

			for each ( track in endSoundQueue ) {
				track.isActive = false;
			}
			endSoundQueue = new Array ();

			for each ( track in randomiseSoundQueue ) {
				track.makeLoop ( loopLength );
			}
			randomiseSoundQueue = new Array ();
		}


		private function updatePlayheadInterval () : void {
			clearInterval ( playheadInterval );
			playheadInterval = setInterval ( playCue, beatDuration );
		}


		private function getTrackByID ( trackID : String ) : Track {
			var result : Track;
			for each ( var track : Track in tracks ) {
				if ( track.id == trackID ) {
					result = track;
					break;
				}
			}
			return result;
		}
	}
}
