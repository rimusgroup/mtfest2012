package com.mtfest.model.soundcue.message {


	/**
	 * @author rimaskrivickas
	 */
	public class PlayingCuePoint {


		private var _currentNote : uint;


		public function PlayingCuePoint ( currentNote : uint ) {
			_currentNote = currentNote;
		}


		public function get currentNote () : uint {
			return _currentNote;
		}
	}
}
