package com.mtfest.model.soundcue.message {


	/**
	 * @author rimaskrivickas
	 */
	public class TrackNoteTriggered {


		private var _trackID : String;


		public function TrackNoteTriggered ( trackID : String ) {
			_trackID = trackID;
		}


		public function get trackID () : String {
			return _trackID;
		}

	}
}
