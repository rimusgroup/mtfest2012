package com.mtfest.view.setuppage.message {


	/**
	 * @author rimaskrivickas
	 */
	public class SetupComplete {


		private var _tag : String;


		public function SetupComplete ( tag : String ) {
			_tag = tag;
		}


		public function get tag () : String {
			return _tag;
		}

	}
}
