package com.mtfest.view.setuppage {


	import com.mtfest.view.setuppage.message.SetupComplete;


	/**
	 * @author rimaskrivickas
	 */
	public class SetupPagePM {


		[MessageDispatcher]
		public var dispatch : Function;


		public function finishSetup ( tag : String ) : void {
			dispatch ( new SetupComplete ( tag ) );
		}
	}
}
