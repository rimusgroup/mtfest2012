package com.mtfest.view.player {


	/**
	 * @author rimaskrivickas
	 */
	public class PlayerItemStates {


		public static const ON : String = "on";

		public static const OFF : String = "off";

		public static const PENDING : String = "pending";
	}
}
