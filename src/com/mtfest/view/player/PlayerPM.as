package com.mtfest.view.player {


	import com.mtfest.view.player.entity.PlayerItem;
	import com.mtfest.view.player.message.SetPlayerItemState;
	import com.mtfest.view.player.message.SetPlayerItems;
	import com.mtfest.view.player.message.TriggerPlayerItem;

	import mx.collections.ArrayCollection;


	/**
	 * @author rimaskrivickas
	 */
	public class PlayerPM {


		[MessageDispatcher]
		public var dispatch : Function;
		
		[Bindable]
		public var data : ArrayCollection;


		// ---------------------------------------------------------------------------------------
		// Public API
		// ---------------------------------------------------------------------------------------
		[MessageHandler]
		public function onSetPlayerItemState ( message : SetPlayerItemState ) : void {
			var playerItem : PlayerItem = getPlayerItemByID ( message.itemID );
			if ( playerItem != null ) {
				playerItem.currentState = message.state;
				playerItem.initiatorName = message.initiatorName;
				playerItem.initiatorImage = message.initiatorImage;
			}
		}
		
		[MessageHandler]
		public function onSetPlayerItems ( message : SetPlayerItems ) : void {
			data = message.items;
			data.refresh();
		}
		
		
		[MessageHandler]
		public function onTriggerPlayerItem ( message : TriggerPlayerItem ) : void {
			var playerItem : PlayerItem = getPlayerItemByID ( message.itemID );
			if ( playerItem != null ) {
				if ( playerItem.animateTrigger != null ) playerItem.animateTrigger();
			}
		}
		
		// ---------------------------------------------------------------------------------------
		// Internal logic
		// ---------------------------------------------------------------------------------------
		private function getPlayerItemByID ( itemID : String ) : PlayerItem {
			var result : PlayerItem;
			for each ( var item : PlayerItem in data ) {
				if ( item.id == itemID ) {
					result = item;
					break;
				}
			}
			return result;
		}
	}
}
