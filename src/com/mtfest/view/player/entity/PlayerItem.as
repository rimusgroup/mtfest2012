package com.mtfest.view.player.entity {


	/**
	 * @author rimaskrivickas
	 */
	public class PlayerItem {


		private var _id : String;

		[Bindable]
		public var currentState : String;

		[Bindable]
		public var initiatorName : String;

		[Bindable]
		public var initiatorImage : String;

		public var animateTrigger : Function;


		public function PlayerItem ( id : String ) {
			_id = id;
		}


		public function get id () : String {
			return _id;
		}
	}
}
