package com.mtfest.view.player.message {


	/**
	 * @author rimaskrivickas
	 */
	public class TriggerPlayerItem {


		private var _itemID : String;


		public function TriggerPlayerItem ( itemID : String ) {
			_itemID = itemID;
		}


		public function get itemID () : String {
			return _itemID;
		}

	}
}
