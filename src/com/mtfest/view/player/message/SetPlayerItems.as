package com.mtfest.view.player.message {
	import mx.collections.ArrayCollection;


	/**
	 * @author rimaskrivickas
	 */
	public class SetPlayerItems {


		private var _items : ArrayCollection;


		public function SetPlayerItems ( items : ArrayCollection ) {
			_items = items;
		}


		public function get items () : ArrayCollection {
			return _items;
		}

	}
}
