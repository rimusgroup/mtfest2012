package com.mtfest.view.player.message {


	/**
	 * @author rimaskrivickas
	 */
	public class SetPlayerItemState {


		private var _state : String;

		private var _itemID : String;
		
		private var _initiatorName : String;

		private var _initiatorImage : String;


		public function SetPlayerItemState ( itemID : String, state : String, initiatorName : String = null, initiatorImage : String = null ) {
			_itemID = itemID;
			_state = state;
			_initiatorName = initiatorName;
			_initiatorImage = initiatorImage;
		}


		public function get state () : String {
			return _state;
		}


		public function get itemID () : String {
			return _itemID;
		}


		public function get initiatorName () : String {
			return _initiatorName;
		}


		public function get initiatorImage () : String {
			return _initiatorImage;
		}

	}
}
