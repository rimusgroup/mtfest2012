package com.mtfest.view.background {


	import com.mtfest.utils.Utils;
	import com.mtfest.view.background.message.UpdateBackground;


	/**
	 * @author rimaskrivickas
	 */
	public class BackgroundPM {


		// ---------------------------------------------------------------------------------------
		// Assets
		// ---------------------------------------------------------------------------------------
		[Embed(source="/images/general/main_bg.png")]
		[Bindable]
		public var mainBackground : Class;
		
		[Embed(source="/images/general/track_titles.png")]
		[Bindable]
		public var trackTitles : Class;

		[Embed(source="/images/backgrounds/bg_01.jpg")]
		public var bg_01 : Class;

		[Embed(source="/images/backgrounds/bg_02.jpg")]
		public var bg_02 : Class;

		[Embed(source="/images/backgrounds/bg_03.jpg")]
		public var bg_03 : Class;

		[Embed(source="/images/backgrounds/bg_04.jpg")]
		public var bg_04 : Class;

		[Embed(source="/images/backgrounds/bg_05.jpg")]
		public var bg_05 : Class;

		[Embed(source="/images/backgrounds/bg_06.jpg")]
		public var bg_06 : Class;

		[Embed(source="/images/backgrounds/bg_07.jpg")]
		public var bg_07 : Class;

		[Embed(source="/images/backgrounds/bg_08.jpg")]
		public var bg_08 : Class;
		
		[Embed(source="/images/backgrounds/bg_09.jpg")]
		public var bg_09 : Class;
		
		[Embed(source="/images/backgrounds/bg_10.jpg")]
		public var bg_10 : Class;
		
		[Embed(source="/images/backgrounds/bg_11.jpg")]
		public var bg_11 : Class;
		
		[Embed(source="/images/backgrounds/bg_12.jpg")]
		public var bg_12 : Class;
		
		[Embed(source="/images/backgrounds/bg_13.jpg")]
		public var bg_13 : Class;
		
		[Embed(source="/images/backgrounds/bg_14.jpg")]
		public var bg_14 : Class;
		
		[Embed(source="/images/backgrounds/bg_15.jpg")]
		public var bg_15 : Class;
		
		[Embed(source="/images/backgrounds/bg_16.jpg")]
		public var bg_16 : Class;

		// ---------------------------------------------------------------------------------------
		// Public parameters
		// ---------------------------------------------------------------------------------------
		[Bindable]
		public var dynamicBackground : Class;

		// ---------------------------------------------------------------------------------------
		// Private parameters
		// ---------------------------------------------------------------------------------------
		private var availableBackgrounds : Vector.<Class>;


		// ---------------------------------------------------------------------------------------
		// Constructor
		// ---------------------------------------------------------------------------------------
		public function BackgroundPM () {
			availableBackgrounds = new Vector.<Class> ();
			availableBackgrounds.push ( bg_01 );
			availableBackgrounds.push ( bg_02 );
			availableBackgrounds.push ( bg_03 );
			availableBackgrounds.push ( bg_04 );
			availableBackgrounds.push ( bg_05 );
			availableBackgrounds.push ( bg_06 );
			availableBackgrounds.push ( bg_07 );
			availableBackgrounds.push ( bg_08 );
			availableBackgrounds.push ( bg_09 );
			availableBackgrounds.push ( bg_10 );
			availableBackgrounds.push ( bg_11 );
			availableBackgrounds.push ( bg_12 );
			availableBackgrounds.push ( bg_13 );
			availableBackgrounds.push ( bg_14 );
			availableBackgrounds.push ( bg_15 );
			availableBackgrounds.push ( bg_16 );
		}


		// ---------------------------------------------------------------------------------------
		// Public API
		// ---------------------------------------------------------------------------------------
		[MessageHandler]
		public function onUpdateBackground ( message : UpdateBackground ) : void {
			if ( availableBackgrounds.length >= 1 ) dynamicBackground = availableBackgrounds [ Utils.randRange ( 0, availableBackgrounds.length - 1 )];
		}
	}
}
